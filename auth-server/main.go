package main

import (
	"database/sql"
	authlogic "example/auth-server/auth-logic"
	"fmt"
	"log"
	"os"

	_ "github.com/lib/pq"
)

func main() {
	var err error
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=%s sslrootcert=%s sslcert=%s sslkey=%s", os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_USER"), os.Getenv("DB_NAME"), os.Getenv("DB_SSLMODE"), os.Getenv("DB_SSLROOTCERT"), os.Getenv("DB_SSLCERT"), os.Getenv("DB_SSLKEY"))
	authlogic.DB, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		log.Panic(err)
	}
	authlogic.Start()
}
