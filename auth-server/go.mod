module example/auth-server

go 1.19

require (
	github.com/golang-jwt/jwt/v4 v4.4.3
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.7
	golang.org/x/crypto v0.5.0
)
