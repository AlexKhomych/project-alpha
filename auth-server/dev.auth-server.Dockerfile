FROM pre.golang-alpine:latest as builder
WORKDIR /app
COPY go.mod go.sum ./
RUN go mod tidy
COPY . .
RUN go build -o main


FROM scratch
COPY --from=builder /app/main /app/main
COPY --from=builder /app/certs /app/certs
ENTRYPOINT ["/app/main"]