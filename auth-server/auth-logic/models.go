package authlogic

type User struct {
	Id       int64  `json:"id" db:"id"`
	Name     string `json:"name" db:"name"`
	Password string `json:"password" db:"password"`
	Token    string `json:"token" db:"token"`
	IsAdmin  bool   `json:"admin" db:"is_admin"`
}
