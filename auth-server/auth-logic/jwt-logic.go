package authlogic

import (
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/golang-jwt/jwt/v4"
)

func createToken(user_id int64) string {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub": user_id,
		"iat": time.Now().Unix(),
	})

	tokenString, err := token.SignedString([]byte(os.Getenv("TOKEN_KEY")))
	if err != nil {
		log.Panic(err)
	}
	query := "UPDATE ng.api_user SET token=$1 WHERE id=$2"
	_, err = DB.Query(query, tokenString, user_id)
	// TODO check if token was created
	if err != nil {
		log.Panic(err)
	}

	return tokenString
}

func validate(tokenString string, isAdmin string) (string, int) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("TOKEN_KEY")), nil
	})
	if err != nil {
		return "Parse Error", http.StatusBadRequest
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		return "Invalid Token", http.StatusBadRequest
	}

	query := "SELECT id, is_admin FROM ng.api_user WHERE id=$1 AND token=$2"
	row, err := DB.Query(query, claims["sub"], tokenString)
	defer row.Close()
	if err != nil {
		log.Panic(err)
	}
	var id int64 = 0
	var is_admin bool = false
	for row.Next() {
		err := row.Scan(&id, &is_admin)
		if err != nil {
			log.Panic(err)
		}
		break
	}
	if id == 0 {
		return "Invalid Token", http.StatusBadRequest
	}
	if len(isAdmin) > 0 && strings.ToLower(isAdmin) == "true" && !is_admin {
		return "Unauthorized", http.StatusUnauthorized
	}
	return "Valid", http.StatusOK
}
