package authlogic

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
)

var DB *sql.DB

func Start() {
	sRouter := mux.NewRouter()
	sRouter.HandleFunc("/signup", signUp).Methods("POST")
	sRouter.HandleFunc("/login", logIn).Methods("POST")
	sRouter.HandleFunc("/new", recreateToken).Methods("POST")
	sRouter.HandleFunc("/validate", validateToken).Methods("POST")
	listenAddr := fmt.Sprintf("0.0.0.0:%s", os.Getenv("AUTH_PORT"))
	fmt.Println("Listenning on ", listenAddr)
	log.Fatal(http.ListenAndServe(listenAddr, sRouter))
}

// ////////////////////////////////////////////////////////////////////////////////
func isExist(user *User) bool {
	query := "SELECT password FROM ng.api_user WHERE LOWER(name)=LOWER($1)"
	row, err := DB.Query(query, user.Name)
	defer row.Close()
	if err != nil {
		log.Panic(err)
	}
	var password string
	for row.Next() {
		err := row.Scan(&password)
		if err != nil {
			log.Panic(err)
		}
		break
	}
	return len(password) != 0
}

// ////////////////////////////////////////////////////////////////////////////////
func recreateToken(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	var user User
	json.NewDecoder(r.Body).Decode(&user)
	if !isExist(&user) {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("User Doesn't exist")
		return
	}
	query := "SELECT id, password FROM ng.api_user WHERE LOWER(name)=LOWER($1)"
	row, err := DB.Query(query, user.Name)
	defer row.Close()
	if err != nil {
		log.Fatal(err)
	}
	var hashpswd string
	for row.Next() {
		err := row.Scan(&user.Id, &hashpswd)
		if err != nil {
			log.Panic(err)
		}
		break
	}

	err = bcrypt.CompareHashAndPassword([]byte(hashpswd), []byte(user.Password))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Wrong password")
		return
	}
	token := createToken(user.Id)
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(token)
}

// ////////////////////////////////////////////////////////////////////////////////
func signUp(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	var user User
	json.NewDecoder(r.Body).Decode(&user)
	if isExist(&user) {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("User already exist")
		return
	}
	password := []byte(user.Password)
	if len(password) > 72 {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Password length exceeds 72 bytes")
		return
	}
	hashpswd, err := bcrypt.GenerateFromPassword(password, 10)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Couldnt generate password hash")
		return
	}
	query := "INSERT INTO ng.api_user(name, password, is_admin) VALUES(LOWER($1), $2, FALSE) RETURNING id"
	row, err := DB.Query(query, user.Name, string(hashpswd))
	defer row.Close()
	if err != nil {
		log.Panic(err)
	}
	for row.Next() {
		err := row.Scan(&user.Id)
		if err != nil {
			log.Panic(err)
		}
		break
	}
	token := createToken(user.Id)
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(token)
}

// ////////////////////////////////////////////////////////////////////////////////
func logIn(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	var user User
	json.NewDecoder(r.Body).Decode(&user)
	if !isExist(&user) {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("No such user")
		return
	}
	query := "SELECT password, token FROM ng.api_user WHERE LOWER(name)=LOWER($1)"
	row, err := DB.Query(query, user.Name)
	defer row.Close()
	if err != nil {
		log.Panic(err)
	}

	var hashpswd string
	var token string
	for row.Next() {
		err := row.Scan(&hashpswd, &token)
		if err != nil {
			log.Panic(err)
		}
		break
	}

	err = bcrypt.CompareHashAndPassword([]byte(hashpswd), []byte(user.Password))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Wrong password")
		// TODO valid attempts
		return
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(token)
}

func validateToken(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	tokenString := r.URL.Query().Get("token")
	isAdmin := r.URL.Query().Get("admin")
	if len(tokenString) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Missing Token")
		return
	}
	msg, status := validate(tokenString, isAdmin)
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(msg)
}
