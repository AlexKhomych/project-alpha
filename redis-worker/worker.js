const fs = require("node:fs");
const { exit } = require("node:process");
const wait = require("node:timers/promises").setTimeout;
const redis = require("redis");
require("fetch");

const wait_a = Number(process.env.REDIS_KEY_EXPIRE_A);
const wait_b = Number(process.env.REDIS_KEY_EXPIRE_B);
const key_exp_total = wait_a + wait_b + 10;

const client = redis.createClient({
  url: `rediss://${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`,
  socket: {
    tls: true,
    key: fs.readFileSync(process.env.REDIS_TLS_KEY, encoding = 'ascii'),
    cert: fs.readFileSync(process.env.REDIS_TLS_CERT, encoding = 'ascii'),
    ca: fs.readFileSync(process.env.REDIS_TLS_CA, encoding = 'ascii')
  }
});

client.connect()
  .catch(err => {
    console.log(err);
    exit(1);
  });
console.log(`Redis is connected on ${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`);

async function setJSONCache(key, value) {
  await client.json.set(key, "$", value)
  await client.expire(key, key_exp_total);
  console.log(`${new Date().toLocaleString()}\t${key} has been set`);
}

async function getGods() {
  try {
    await fetch(`http://${process.env.REST_API_HOST}/gods?token=${process.env.REST_API_TOKEN}`)
      .then(res => res.json())
      .then(gods => {
        god_names = [];
        for (const god of gods) {
          setJSONCache(god.name.toLowerCase(), god);
          god_names.push(god.name.charAt(0).toUpperCase() + god.name.slice(1).toLowerCase())
        }
        setJSONCache("god_names", god_names);
      });
  } catch (err) {
    console.log(err);
  }
}

// Run worker indefinetely
(async () => {
  while (true) {
    await wait(wait_a * 1000);
    await getGods();
    await wait(wait_b * 1000);
  }
})();