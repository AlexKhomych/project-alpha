FROM node:19-alpine3.16

WORKDIR /redis-worker

COPY package*.json ./
RUN npm ci --only=production


COPY . .
RUN chown -R node:node /redis-worker

USER node

CMD ["npm", "run", "init"]