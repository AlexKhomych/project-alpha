# Project Alpha

## About Me
I am Alex, having a versatile journey, where I apply and try many things. Made a couple of
projects in past year such as Java Swing GUI, Discord Bot, basic Rest API in Golang and JavaScript. Learned quite a few tools and design patterns.

## Project idea
Apply everything I've learned into one project, seems messy? Yes, but I did try to make it more readable. 
Here I tried to develop Discord Bot with JS that uses REST API written in GO. The API has data about Norse Gods, while Bot uses it to display in Discord and more. Redis cache layer in front of API for Discord Bot, where clients may require a lot of requests. HAProxy in front of REST API, that uses the most basic configuration. That's all packed into docker, applied with use of self-signed TLS certificates and keeping secure information - secure. It utilizes Postgres behind the scenes, which uses PgBouncer for application connections. Oh and one more, REST API is stateless and therefore Auth Server that uses JWT was separated.
While it is not complete and has flaws and ways to improve. I think it suits perfectly for a Project Alpha that once I may look back and get that popular nostalgia.

- [x] Try something new
- [x]  Have fun
- [x] Make it happen
- [ ] Celebrate 🥳

## Installation
Before installation, Postgres is required. I used v15
You must setup connection availability as any security measures needed in your case
This is SQL queries needed for creation of tables
```sql
/* CLEAN UP

DROP SCHEMA ng CASCADE;
DROP ROLE api;
*/

CREATE SCHEMA ng;
ALTER DEFAULT PRIVILEGES IN SCHEMA ng REVOKE ALL ON TABLES FROM PUBLIC;
CREATE ROLE api WITH LOGIN;

CREATE TABLE ng.god(
id BIGSERIAL PRIMARY KEY,
name TEXT NOT NULL UNIQUE,
aliases TEXT[],
parents TEXT[],
siblings TEXT[],
wedded text,
children TEXT[],
traits TEXT[]
);

CREATE INDEX index_god_name ON ng.god(name);

INSERT INTO ng.god
(name, aliases, parents, siblings, wedded, children, traits)
VALUES
('odin', '{"all-father"}', '{"Bestla", "Borr"}', '{"Vili", "Ve"}', 'Frigg', '{"Baldur","Thor","Tyr","Heimdall","Vali","Vidarr","Hodr","Bragi"}', '{"wit", "wile", "wisdom", "shapeshifter"}'),
('thor', '{"thunder"}', '{"Odin", "Jord"}', '{"Baldur", "Heimdall", "Tyr", "Váli", "Vidarr", "Bragi", "Hodr"}', 'Sif', '{"Thudr", "Magni"}', '{"brave", "strong", "fierce"}');

CREATE ROLE replicator WITH REPLICATION LOGIN ENCRYPTED PASSWORD 'your-password';
  
CREATE TABLE ng.api_user(
id BIGSERIAL PRIMARY KEY,
name TEXT NOT NULL UNIQUE,
password TEXT NOT NULL,
token TEXT,
is_admin BOOLEAN NOT NULL
);

GRANT USAGE ON SCHEMA ng TO api;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA ng TO api;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA ng TO api;
```
Once database setup is done. 
Project may be cloned after what next steps must be performed:
1. Create env-files folder in main project directory
It must have corresponding env-files which will be used for docker compose:
- auth-server.env
- golang-api-server.env
- redis-worker.env
- discord-bot.env
- redis-stack-server.env
2. Populate those files with corresponding key-values:
	auth-server.env
	
	AUTH_PORT - Port on which application will be serving
	
	DB_PORT
	DB_HOST
	DB_USER
	DB_NAME
	DB_SSLMODE
	DB_SSLROOTCERT
	DB_SSLCERT
	DB_SSLKEY
	TOKEN_KEY  - Signing secret for JWT

	golang-api-server.env
	
	DB_PORT
	DB_HOST
	DB_USER
	DB_NAME
	DB_SSLMODE
	DB_SSLROOTCERT
	DB_SSLCERT
	DB_SSLKEY
	
	AUTH_PORT - port on which auth-server is serving

	redis-worker.env
	
	REST_API_HOST
	REST_API_TOKEN - Token to authenticate to REST API
	
	REDIS_HOST
	REDIS_PORT
	REDIS_TLS_KEY
	REDIS_TLS_CERT
	REDIS_TLS_CA
	REDIS_KEY_EXPIRE_A - Time in seconds recommend 20
	REDIS_KEY_EXPIRE_B - Time in seconds recommend 30
	
	NODE_ENV=production

	discord-bot.env
	
	BOT_TOKEN - Discord Bot Token
	CLIENT_ID - ID of the Bot
	GUILD_ID - Guild ID used to deploy new commands or clear them (deploy_commands.js)
	
	REST_API_HOST
	REST_API_TOKEN - Token to authenticate to REST API
	
	REDIS_HOST
	REDIS_PORT
	REDIS_TLS_KEY
	REDIS_TLS_CERT
	REDIS_TLS_CA
	
	NODE_ENV=production%  

	redis-stack-server.env
	
	REDIS_ARGS="--tls-port 7379 --port 0 --tls-ca-cert-file "YOUR CA" --tls-cert-file "YOUR CERT" --tls-key-file "YOUR KEY" --tls-auth-clients yes"

Redis args may be ommited if you do not use TLS and want to stick to defaults.
3. Created needed certificates, same authority and with SAN - DNS extension:
- ca.crt - CA certificate for verify-full
- postgres-api.crt - postgres client certificate
- postgres-api.key - postgres client key
- redis.crt - redis certificate
- redis.key - redis key
4. Once needed certificates are generated, we have to place them in corresponding directories so applications can use them (You aswell may reference .gitlab-ci.yml)
- auth-server/certs/ must have CA and Postgres Client Certificates
- discord-bot/redis-certs/ CA and Redis Certificates
- go-api/certs/ CA and Postgres Client Certificates
- redis-worker/redis-certs/ CA and Redis Certificates
5. Feel free to take a look on application code and change options to suit your needs.

Remember my design was intended to utilize TLS, therefore all applications uses certificates.

## Usage
### REST API
It has following endpoints:
- /
- /health
- /signup
- /login
- /new
- /gods?token=\[Your Token\]
- /gods/name/{God Name}?token=\[Your Token\]
- /gods/id/{id}
From which **signup, login, new** - requires **POST in JSON** format:
```json
{
"name": "user",
"password": "abc123"
}
```
**/gods** endpoint supports **GET | POST | PUT** example of data:
```json
{
    "id": 2,
    "name": "thor",
    "aliases": [
        "thunder"
    ],
    "parents": [
        "Odin",
        "Jord"
    ],
    "siblings": [
        "Baldur",
        "Heimdall",
        "Tyr",
        "Váli",
        "Vidarr",
        "Bragi",
        "Hodr"
    ],
    "wedded": "Sif",
    "children": [
        "Thudr",
        "Magni"
    ],
    "traits": [
        "brave",
        "strong",
        "fierce"
    ]
}
```
P.s for PUT, either **id** or **name** must be specified
**/gods/id/{id}** supports **GET | DELETE**

#### NOTE:
For data modification such as **POST | PUT | DELETE** **is_admin** column for the user making request in Postgres table ng.api_users must be **Set to TRUE**

### Discord Bot
Supports following slash commands:
- setup
- norse_god
- god_role_select
- god_role_color
- not_ping (for fun)
#### NOTE:
- setup needed if you wish to utilize god_role_* slash commands - it creates 2 roles which are hard coded in setup.js - Odin and Thor. If those roles do not exists, the bot will try to create them. The check if role exists performed on the lowercase name.
- god_role_color requires hex code without # symbol as well as god for which you wish to change the color. If ommited the color, default black team color will be set.
- god_role_select is used to create select menu in the channel the command was executed. It uses dynamic options that are fetched from Redis that is regularly populated by redis worker. Due to hard coded roles Odin and Thor by setup, those if specified in select menu will be working as expected, while any other database entry for a new gode will not take effect and simply reply unavailability to retrive the role to the user. Surely you may create that role manually using newly create god's name.

## License
Uhm I dunno what is it. Well it is free to use and so on. Credits would be appreciated.
