package models

type HealthStatus struct {
	APP_ID   string `json:"app_id"`
	DBStatus string `json:"db_status"`
}
