package models

type Psql_Info struct {
	Host        string
	Port        string
	User        string
	Password    string
	DBname      string
	SSLmode     string
	SSLrootcert string
	SSLcert     string
	SSLkey      string
}
