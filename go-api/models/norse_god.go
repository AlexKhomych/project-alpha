package models

import (
	"github.com/lib/pq"
)

type NorseGod struct {
	ID       int64          `json:"id" db:"id"`
	Name     string         `json:"name" db:"name"`
	Aliases  pq.StringArray `json:"aliases" db:"aliases"`
	Parents  pq.StringArray `json:"parents" db:"parents"`
	Siblings pq.StringArray `json:"siblings" db:"siblings"`
	Wedded   string         `json:"wedded" db:"wedded"`
	Children pq.StringArray `json:"children" db:"children"`
	Traits   pq.StringArray `json:"traits" db:"traits"`
}
