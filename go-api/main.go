package main

import (
	"example/testing/api"
	"example/testing/models"
	"example/testing/storage"
	"fmt"
	"os"
)

func main() {
	psql_info := models.Psql_Info{Host: os.Getenv("DB_HOST"), Port: os.Getenv("DB_PORT"), User: os.Getenv("DB_USER"), DBname: os.Getenv("DB_NAME"), SSLmode: os.Getenv("DB_SSLMODE"), SSLrootcert: os.Getenv("DB_SSLROOTCERT"), SSLcert: os.Getenv("DB_SSLCERT"), SSLkey: os.Getenv("DB_SSLKEY")}
	db := storage.NewPostgresDB(psql_info)
	listenAddr := fmt.Sprintf("0.0.0.0:%s", os.Getenv("REST_API_PORT"))
	server := api.NewServer(listenAddr, db)
	server.Start()
}
