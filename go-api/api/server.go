package api

import (
	"bytes"
	"encoding/json"
	"example/testing/models"
	"example/testing/storage"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

type Server struct {
	listenAddr string
	db         storage.Storage
}

func NewServer(listenAddr string, db storage.Storage) *Server {
	return &Server{
		listenAddr: listenAddr,
		db:         db,
	}
}

func (s *Server) Start() {
	s.db.Connect()
	sRouter := mux.NewRouter()
	sRouter.HandleFunc("/", s.getHome).Methods("GET")
	sRouter.HandleFunc("/health", s.getHealth).Methods("GET")
	sRouter.HandleFunc("/signup", s.signUp).Methods("POST")
	sRouter.HandleFunc("/login", s.logIn).Methods("POST")
	sRouter.HandleFunc("/new", s.new).Methods("POST")
	sRouter.Handle("/gods", s.validate(s.getGods)).Methods("GET")
	sRouter.Handle("/gods/id/{id}", s.validate(s.getGodByID)).Methods("GET")
	sRouter.Handle("/gods/name/{name}", s.validate(s.getGodByName)).Methods("GET")
	sRouter.Handle("/gods", s.validateAdmin(s.addGod)).Methods("POST")
	sRouter.Handle("/gods", s.validateAdmin(s.updateGod)).Methods("PUT")
	sRouter.Handle("/gods/id/{id}", s.validateAdmin(s.removeGodByID)).Methods("DELETE")
	log.Fatal(http.ListenAndServe(s.listenAddr, sRouter))
}

func (s *Server) getHome(w http.ResponseWriter, r *http.Request) {
	welcome := "Welcome!"
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(welcome)
}

func (s *Server) getHealth(w http.ResponseWriter, r *http.Request) {
	var dbstatus string
	if s.db.IsAvailable() {
		dbstatus = "Online"
	} else {
		dbstatus = "Offline"
	}
	healthStatus := models.HealthStatus{APP_ID: os.Getenv("APP_ID"), DBStatus: dbstatus}
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(healthStatus)
}

func (s *Server) signUp(w http.ResponseWriter, r *http.Request) {
	resBody, status := requestAuth("signup", "application/json", r)
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(string(resBody[1 : len(resBody)-2]))
}

func (s *Server) logIn(w http.ResponseWriter, r *http.Request) {
	resBody, status := requestAuth("login", "application/json", r)
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(string(resBody[1 : len(resBody)-2]))
}

func (s *Server) new(w http.ResponseWriter, r *http.Request) {
	resBody, status := requestAuth("new", "application/json", r)
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(string(resBody[1 : len(resBody)-2]))
}

func (s *Server) validate(next func(w http.ResponseWriter, r *http.Request)) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Panic(err)
		}
		token := r.URL.Query().Get("token")
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
		resBody, status := requestAuth(fmt.Sprintf("validate?token=%s", token), "application/json", r)
		if status != http.StatusOK {
			json.NewEncoder(w).Encode(string(resBody[1 : len(resBody)-2]))
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
		next(w, r)
	})
}

func (s *Server) validateAdmin(next func(w http.ResponseWriter, r *http.Request)) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Panic(err)
		}
		token := r.URL.Query().Get("token")
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
		resBody, status := requestAuth(fmt.Sprintf("validate?token=%s&admin=true", token), "application/json", r)
		if status != http.StatusOK {
			json.NewEncoder(w).Encode(string(resBody[1 : len(resBody)-2]))
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
		next(w, r)
	})
}

func requestAuth(endpoint string, contentType string, r *http.Request) ([]byte, int) {
	requestURL := fmt.Sprintf("http://auth-server:%s/%s", os.Getenv("AUTH_PORT"), endpoint)
	res, err := http.Post(requestURL, contentType, r.Body)
	if err != nil {
		log.Panic(err)
	}
	defer res.Body.Close()
	resBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Panic(err)
	}
	return resBody, res.StatusCode
}

func (s *Server) getGods(w http.ResponseWriter, r *http.Request) {
	norseGods := s.db.GetGods()
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(norseGods)
}

func (s *Server) getGodByID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	norseGod := s.db.GetGodById(id)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(norseGod)
}

func (s *Server) getGodByName(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	name := vars["name"]
	norseGod := s.db.GetGodByName(name)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(norseGod)
}

func (s *Server) addGod(w http.ResponseWriter, r *http.Request) {
	var norseGod models.NorseGod
	json.NewDecoder(r.Body).Decode(&norseGod)
	res := s.db.PostGod(&norseGod)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(res)
}

func (s *Server) updateGod(w http.ResponseWriter, r *http.Request) {
	var norseGod models.NorseGod
	json.NewDecoder(r.Body).Decode(&norseGod)
	log.Println("Updating God", norseGod.Name)
	res := s.db.UpdateGod(&norseGod)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(res)
}

func (s *Server) removeGodByID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	god_id := vars["id"]
	id := s.db.DeleteGod(god_id)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(id)
}
