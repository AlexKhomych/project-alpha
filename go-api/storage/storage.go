package storage

import "example/testing/models"

type Storage interface {
	Connect()
	IsAvailable() bool
	GetGods() []models.NorseGod
	GetGodById(string) *models.NorseGod
	GetGodByName(string) *models.NorseGod
	PostGod(*models.NorseGod) string
	UpdateGod(*models.NorseGod) string
	DeleteGod(string) string
}
