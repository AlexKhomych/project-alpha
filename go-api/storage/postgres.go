package storage

import (
	"database/sql"
	"example/testing/models"
	"fmt"
	"log"
	"strconv"

	"github.com/lib/pq"
)

type PostgresDB struct {
	db        *sql.DB
	psql_info models.Psql_Info
}

func NewPostgresDB(psql_info models.Psql_Info) *PostgresDB {
	postgresDB := PostgresDB{psql_info: psql_info}
	return &postgresDB
}

func (s *PostgresDB) Connect() {
	conninfo := s.psql_info
	var err error
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=%s sslrootcert=%s sslcert=%s sslkey=%s", conninfo.Host, conninfo.Port, conninfo.User, conninfo.DBname, conninfo.SSLmode, conninfo.SSLrootcert, conninfo.SSLcert, conninfo.SSLkey)
	s.db, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		log.Panic(err)
	}
}

func (s *PostgresDB) IsAvailable() bool {
	err := s.db.Ping()
	return err == nil
}

func (s *PostgresDB) GetGods() []models.NorseGod {
	query := "SELECT * FROM ng.god"
	rows, err := s.db.Query(query)
	defer rows.Close()
	if err != nil {
		log.Panic(err)
	}
	var norseGods []models.NorseGod
	for rows.Next() {
		var norseGod models.NorseGod
		err = rows.Scan(&norseGod.ID, &norseGod.Name, &norseGod.Aliases, &norseGod.Parents, &norseGod.Siblings, &norseGod.Wedded, &norseGod.Children, &norseGod.Traits)
		if err != nil {
			log.Panic(err)
		}
		norseGods = append(norseGods, norseGod)
	}
	return norseGods
}

func (s *PostgresDB) GetGodById(id string) *models.NorseGod {
	query := "SELECT * FROM ng.god WHERE id=$1"
	row, err := s.db.Query(query, id)
	defer row.Close()
	if err != nil {
		log.Panic(err)
	}
	var norseGod models.NorseGod
	for row.Next() {
		err := row.Scan(&norseGod.ID, &norseGod.Name, &norseGod.Aliases, &norseGod.Parents, &norseGod.Siblings, &norseGod.Wedded, &norseGod.Children, &norseGod.Traits)
		if err != nil {
			log.Panic(err)
		}
		break
	}

	return &norseGod
}

func (s *PostgresDB) GetGodByName(name string) *models.NorseGod {
	query := "SELECT * FROM ng.god WHERE LOWER(name)=LOWER($1)"
	row, err := s.db.Query(query, name)
	defer row.Close()
	if err != nil {
		log.Panic(err)
	}
	var norseGod models.NorseGod
	for row.Next() {
		err := row.Scan(&norseGod.ID, &norseGod.Name, &norseGod.Aliases, &norseGod.Parents, &norseGod.Siblings, &norseGod.Wedded, &norseGod.Children, &norseGod.Traits)
		if err != nil {
			log.Panic(err)
		}
		break
	}

	return &norseGod
}

func (s *PostgresDB) PostGod(norseGod *models.NorseGod) string {
	if s.isExistName(norseGod.Name) != 0 {
		return "God with that name already exists"
	}
	query := "INSERT INTO ng.god(name, aliases, parents, siblings, wedded, children, traits) VALUES(LOWER($1), $2, $3, $4, $5, $6, $7) RETURNING id"
	row, err := s.db.Query(query, norseGod.Name, pq.Array(norseGod.Aliases), pq.Array(norseGod.Parents), pq.Array(norseGod.Siblings), norseGod.Wedded, pq.Array(norseGod.Children), pq.Array(norseGod.Traits))
	defer row.Close()
	if err != nil {
		log.Panic(err)
	}
	var god_id int64
	for row.Next() {
		err := row.Scan(&god_id)
		if err != nil {
			log.Panic(err)
		}
		break
	}

	return strconv.FormatInt(god_id, 10)
}

func (s *PostgresDB) UpdateGod(norseGod *models.NorseGod) string {
	id := s.isExistName(norseGod.Name)
	if id == 0 && norseGod.ID == 0 {
		return "God with that (name|id) doesn't not exists"
	}
	if norseGod.ID == 0 {
		norseGod.ID = id
	}
	NGT := s.GetGodById(strconv.FormatInt(norseGod.ID, 10))
	s.updateGodHelper(NGT, norseGod)
	query := "UPDATE ng.god SET name = LOWER($1), aliases = $2, parents = $3, siblings = $4, wedded = $5, children = $6, traits = $7 WHERE id = $8 RETURNING id"
	row, err := s.db.Query(query, NGT.Name, pq.Array(NGT.Aliases), pq.Array(NGT.Parents), pq.Array(NGT.Siblings), NGT.Wedded, pq.Array(NGT.Children), pq.Array(NGT.Traits), NGT.ID)
	defer row.Close()
	if err != nil {
		log.Panic(err)
	}
	var god_id int64
	for row.Next() {
		err := row.Scan(&god_id)
		if err != nil {
			log.Panic(err)
		}
		break
	}
	return strconv.FormatInt(god_id, 10)
}

func (s *PostgresDB) DeleteGod(id string) string {
	query := "DELETE FROM ng.god WHERE id = $1 RETURNING id"
	row, err := s.db.Query(query, id)
	defer row.Close()
	if err != nil {
		log.Panic(err)
	}
	var god_id int64
	for row.Next() {
		err := row.Scan(&god_id)
		if err != nil {
			log.Panic(err)
		}
		break
	}
	return strconv.FormatInt(god_id, 10)
}

func (s *PostgresDB) isExistName(name string) int64 {
	query := "SELECT id FROM ng.god WHERE name=LOWER($1)"
	row, err := s.db.Query(query, name)
	defer row.Close()
	if err != nil {
		log.Panic(err)
	}
	var god_id int64
	for row.Next() {
		err := row.Scan(&god_id)
		if err != nil {
			log.Panic(err)
		}
		break
	}
	log.Println("GOD_ID", god_id)
	return int64(god_id)
}

// For partial update
func (s *PostgresDB) updateGodHelper(norseGodTarget *models.NorseGod, norseGod *models.NorseGod) {
	if len(norseGod.Name) != 0 {
		norseGodTarget.Name = norseGod.Name
	}
	if norseGod.Aliases != nil {
		norseGodTarget.Aliases = norseGod.Aliases
	}
	if norseGod.Parents != nil {
		norseGodTarget.Parents = norseGod.Parents
	}
	if norseGod.Siblings != nil {
		norseGodTarget.Siblings = norseGod.Siblings
	}
	if len(norseGod.Wedded) != 0 {
		norseGodTarget.Wedded = norseGod.Wedded
	}
	if norseGod.Children != nil {
		norseGodTarget.Children = norseGod.Children
	}
	if norseGod.Traits != nil {
		norseGodTarget.Traits = norseGod.Traits
	}
}
