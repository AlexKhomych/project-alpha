const { StringSelectMenuBuilder } = require("discord.js");
const { capitalize } = require("../utils/string_utils");

module.exports = {
  menu_data: new StringSelectMenuBuilder()
    .setCustomId("god_role_menu")
    .setPlaceholder("Chose a role")
    .addOptions(),
  async execute(interaction) {
    const value = interaction.values[0];
    const value_cap = await capitalize(value);
    const role_id = interaction.client.god_role_select.get(value);
    if (!role_id) {
      await interaction.reply({
        content: `Couldn't find role id for ${value_cap}`,
        ephemeral: true,
      });
      return;
    }
    const guild_memeber = interaction.member;
    const roles = guild_memeber.roles;
    roles.add(role_id, `Select Menu`);
    await interaction.reply({
      content: `${value_cap} role has been successfully added!`,
      ephemeral: true,
    });
  }
}