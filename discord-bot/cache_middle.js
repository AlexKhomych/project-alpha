require("fetch");

module.exports = {
  previous_cached_names: [],
  async getGod(name, redis_client) {
    if (!name) return null;
    if (redis_client) {
      const cache_res = await redis_client.json.get(name.toLowerCase());
      return cache_res;
    }
    const api_response = await fetch(`http://${process.env.REST_API_HOST}/gods/name/${name}?token=${process.env.REST_API_TOKEN}`)
      .catch(err => {
        console.log(err);
        return null;
      });
    const api_res = await api_response.json();
    if (!api_res.god_id || !api_res.name) return null;
    return api_res;
  },
  async getGodNames(redis_client) {
    if (!redis_client) return null;
    const cache_res = await redis_client.json.get("god_names");
    if (!cache_res) {
      return this.previous_cached_names;
    }
    previous_cached_names = cache_res;
    console.log("God names:", cache_res);
    return cache_res;
  }
}