const fs = require("node:fs");
const path = require("node:path");
const { Client, GatewayIntentBits, Collection } = require("discord.js")
const redis = require("redis");

const client = new Client({ intents: [GatewayIntentBits.Guilds] });

client.commands = new Collection();
client.buttons = new Collection();
client.menus = new Collection();

// TODO: Store in the database - perm storage
client.god_role_select = new Collection();

client.redis = redis.createClient({
  url: `rediss://${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`,
  socket: {
    tls: true,
    key: fs.readFileSync(process.env.REDIS_TLS_KEY, encoding = 'ascii'),
    cert: fs.readFileSync(process.env.REDIS_TLS_CERT, encoding = 'ascii'),
    ca: fs.readFileSync(process.env.REDIS_TLS_CA, encoding = 'ascii')
  }
});

client.redis.connect()
  .catch(err => { 
    client.redis = null;
    console.log(err); 
  })
  .then(console.log(`Redis connected on ${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`));

const commandsPath = path.join(__dirname, "commands");
const commandFiles = fs
  .readdirSync(commandsPath)
  .filter((file) => file.endsWith(".js"));

for (const file of commandFiles) {
  const filePath = path.join(commandsPath, file);
  const command = require(filePath);
  client.commands.set(command.data.name, command);
}

const eventsPath = path.join(__dirname, "events");
const eventFiles = fs
  .readdirSync(eventsPath)
  .filter((file) => file.endsWith(".js"));

for (const file of eventFiles) {
  const filePath = path.join(eventsPath, file);
  const event = require(filePath);
  if (event.once) {
    client.once(event.name, (...args) => event.execute(...args));
  } else {
    client.on(event.name, (...args) => event.execute(...args));
  }
}

const buttonsPath = path.join(__dirname, "buttons");
const buttonFiles = fs
  .readdirSync(buttonsPath)
  .filter((file) => file.endsWith(".js"));

for (const file of buttonFiles) {
  const filePath = path.join(buttonsPath, file);
  const button = require(filePath);
  const { button_data } = button;
  const { data } = button_data;
  const { custom_id } = data;
  client.buttons.set(custom_id, button);
}

const menusPath = path.join(__dirname, "select_menus");
const menuFiles = fs
  .readdirSync(menusPath)
  .filter((file) => file.endsWith(".js"));

for (const file of menuFiles) {
  const filePath = path.join(menusPath, file);
  const menu = require(filePath);
  const { menu_data } = menu;
  const { data } = menu_data;
  const { custom_id } = data;
  client.menus.set(custom_id, menu);
}


client.login(process.env.BOT_TOKEN);