const { ButtonBuilder, ButtonStyle, ActionRowBuilder } = require("discord.js")

module.exports = {
  button_data: new ButtonBuilder()
    .setCustomId("click_me")
    .setLabel("Click me please!")
    .setStyle(ButtonStyle.Primary)
    .setDisabled(false),
  async execute(interaction) {
    const { message } = interaction;
    const button_data = message.components[0].components[0].data;
    let [label, msg, isDisabled] = ["", "", false];
    if(button_data.label === "Click me please!"){
      label = "Well done"
      msg = "Really well done!"
    }else {
      label = "Bruh"
      msg = "You already got me -_-"
      isDisabled = true;
    }
    message.edit({
      components: [
        new ActionRowBuilder().addComponents(
          new ButtonBuilder()
            .setCustomId("click_me")
            .setLabel(label)
            .setStyle(ButtonStyle.Success)
            .setDisabled(isDisabled),
        ),
      ]
    });
    
    await interaction.reply({
      content: msg,
    });
  }
}