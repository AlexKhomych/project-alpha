FROM node:19-alpine3.16

WORKDIR /bot

COPY package*.json ./
RUN npm ci --only=production

COPY . .
RUN chown -R node:node /bot

USER node

CMD ["npm", "run", "init"]