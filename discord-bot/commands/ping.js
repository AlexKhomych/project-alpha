const { SlashCommandBuilder, ActionRowBuilder } = require("discord.js");

module.exports = {
  data: new SlashCommandBuilder()
    .setName("not_ping")
    .setDescription("Replies with 'Your Mom!'")
    .addStringOption((option) =>
      option
        .setName("folk")
        .setDescription("PP POOPOO CHECK")
        .setAutocomplete(true)),
  async autocomplete(interaction) {
    const focusedValue = interaction.options.getFocused();
    const choices = ["Your Mom", "Your Dad"];
    const filtered = choices.filter(choice => choice.startsWith(focusedValue));
    await interaction.respond(
      filtered.map(choice => ({ name: choice, value: choice })),
    );
  },
  async execute(interaction) {
    const button = interaction.client.buttons.get("click_me");
    const folk = interaction.options.getString("folk");
    await interaction.reply({
      content: folk,
      components: [
        new ActionRowBuilder().addComponents(
          button.button_data
        ),
      ],
    });
  },
};