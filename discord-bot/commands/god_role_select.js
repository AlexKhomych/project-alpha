const { SlashCommandBuilder, ActionRowBuilder, StringSelectMenuOptionBuilder } = require("discord.js")
const { getGodNames } = require("../cache_middle");
module.exports = {
  data: new SlashCommandBuilder()
    .setName("god_role_select")
    .setDescription("select Norse God Role"),
  async execute(interaction) {
    const menu = interaction.client.menus.get("god_role_menu");
    let component = menu.menu_data;
    const god_names = await getGodNames(interaction.client.redis);
    if (!god_names) {
      await interaction.reply({
        content: "Couldn't retrieve god names",
        ephemeral: true,
      });
      return;
    }
    let options = [];
    for (const god_name of god_names) {
      const ssmb_option = StringSelectMenuOptionBuilder.from({
        label: god_name,
        value: god_name.toLowerCase(),
      });
      options.push(ssmb_option);
    }
    component.options = options;
    await interaction.reply({
      components: [
        new ActionRowBuilder().addComponents(
          component
        ),
      ],
    })
  }
};