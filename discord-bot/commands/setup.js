const { SlashCommandBuilder } = require("discord.js");

module.exports = {
  data: new SlashCommandBuilder()
    .setName("setup")
    .setDescription("Setup bot with need roles"),
  async execute(interaction) {
    await interaction.deferReply({
      ephemeral: true,
    });
    const guild = interaction.guild;
    const roles_cache = guild.roles.cache;
    if (!roles_cache.some(role => {
      if (role.name.toLowerCase() === "thor") {
        interaction.client.god_role_select.set(role.name.toLowerCase(), role.id);
        return true;
      }
    })) {
      guild.roles.create({
        name: 'Thor',
        permissions: []
      });
    }
    if (!roles_cache.some(role => {
      if (role.name.toLowerCase() === "odin") {
        interaction.client.god_role_select.set(role.name.toLowerCase(), role.id);
        return true;
      }
    })) {
      guild.roles.create({
        name: 'Odin',
        permissions: []
      });
    }

    await interaction.editReply({
      content: `Setup is finished`,
    });
  },
}