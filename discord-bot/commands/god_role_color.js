const { SlashCommandBuilder } = require("discord.js");
const { capitalize } = require("../utils/string_utils");
module.exports = {
  data: new SlashCommandBuilder()
    .setName("god_role_color")
    .setDescription("Change color for god role")
    .addStringOption((option) =>
      option
        .setName("god_name")
        .setDescription("God role name")
        .addChoices(
          { name: "Odin", value: "odin" },
          { name: "Thor", value: "thor" },
        )
    ).addStringOption((option) =>
      option
        .setName("color")
        .setDescription("God role hex color code")
    ),
  async execute(interaction) {
    await interaction.deferReply({
      ephemeral: true,
    });
    const roles_cache = interaction.guild.roles.cache;
    const god_name = interaction.options.getString("god_name");
    const god_name_cap = await capitalize(god_name);
    const color = interaction.options.getString("color");
    let message = "";

    if (!roles_cache.some(role => {
      if (role.name.toLowerCase() === god_name) {
        role.setColor(color ? `#${color}` : "#979c9f")
          .catch(err => console.log(err));
        return true;
      }
    })) {
      message = `Couldn't find the role with a name ${god_name_cap}`;
    } else {
      message = `Changed role color for ${god_name_cap}`;
    }

    interaction.editReply({
      content: message,
    });
  }
}