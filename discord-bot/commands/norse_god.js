const { SlashCommandBuilder, EmbedBuilder } = require("discord.js");
const { getGod, getGodNames } = require("../cache_middle.js");
const { capitalize } = require("../utils/string_utils")

module.exports = {
  data: new SlashCommandBuilder()
    .setName("norse_god")
    .setDescription("Get information about selected norse god")
    .addStringOption((option) =>
      option
        .setName("god")
        .setDescription("Select norse god")
        .setAutocomplete(true)),
  async autocomplete(interaction) {
    const choices = await getGodNames(interaction.client.redis);
    if (!choices) return;
    const focusedValue = interaction.options.getFocused();
    const filtered = choices.filter(choice => choice.toLowerCase().startsWith(focusedValue.toLowerCase() || focusedValue));
    await interaction.respond(
      filtered.map(choice => ({ name: choice, value: choice.toLowerCase() })),
    );
  },
  async execute(interaction) {
    let server_bot = interaction.client.users.cache.get(interaction.applicationId);
    const god_name = interaction.options.getString("god");
    const god_name_cap = await capitalize(god_name);
    if (!god_name) {
      await interaction.reply({
        content: `Please specify god name`,
        ephemeral: true,
      });
      return;
    }
    const god = await getGod(god_name, interaction.client.redis);
    if (!god) {
      await interaction.reply({
        content: `Couldn't find specified god with "${god_name_cap}" as a name`,
        ephemeral: true,
      });
      return;
    }

    const god_embed = new EmbedBuilder()
      .setTitle(god_name_cap)
      .setAuthor({ name: `${server_bot.username}`, iconURL: `https://cdn.discordapp.com/avatars/${server_bot.id}/${server_bot.avatar}.png` })
      .setColor(0xFF7C69)
      .setDescription(`Information about ${god_name_cap} god`)
      .addFields(
        { name: "Aliases", value: `${god.aliases ? god.aliases.join(", ") : "undefined"}` },
        { name: "Parents", value: `${god.parents ? god.parents.join(", ") : "undefined"}` },
        { name: "Siblings", value: `${god.siblings ? god.siblings.join(", ") : "undefined"}` },
        { name: "Wedded", value: `${god.wedded ? god.wedded : "undefined"}` },
        { name: "Children", value: `${god.children ? god.children.join(", ") : "undefined"}` },
        { name: "Traits", value: `${god.traits ? god.traits.join(", ") : "undefined"}` },
      )
      .setTimestamp()
    await interaction.reply({
      embeds: [god_embed],
    })
  }
};